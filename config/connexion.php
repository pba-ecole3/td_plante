<?php
    try {
        $dns = "mysql:host=localhost;dbname=tdplante";
        $utilisateur = "root";
        $motDePasse = "";

        $connexion = new PDO($dns, $utilisateur, $motDePasse);
    } catch (\Exception $e) {
        echo "Connexion à MySQL impossible : <br>", $e->getMessage();
        die();
    }
?>