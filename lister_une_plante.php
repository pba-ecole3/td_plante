   <!DOCTYPE html>
<html lang="fr" dir="ltr">
    <?php include "templates/header.php"; ?>
    <body>
        <?php
            require_once("config/connexion.php");
            $select = $connexion->query("SELECT * FROM plante WHERE noplante=" . $_GET["noPlante"]);
            $select->setFetchMode(PDO::FETCH_OBJ);
            $unePlante = $select->fetch();
        ?>


        <?php if ($unePlante): ?>
            <ul class="list-group">
                <li class="list-group-item active"><?= $unePlante->nomplante; ?></li>
                <li class="list-group-item"><?= $unePlante->noplante; ?></li>
                <li class="list-group-item"><?= $unePlante->noregion; ?></li>
            </ul>
        <?php else: ?>
            <h4>Aucun résultat</h4>
        <?php endif; ?>


    <?php include "templates/footer.php"; ?>
    </body>
</html>
 