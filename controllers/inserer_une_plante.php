<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <?php include "templates/header.php"; ?>
    <body>
        <?php
            require_once("config/connexion.php");
                $select = $connexion->query("SELECT * FROM region");
                $select->setFetchMode(PDO::FETCH_OBJ);
        ?>
        <form action="" method="post">
            <label for="txtID" class="form-label">ID :</label>
            <input type="number" name="txtId" class="form-control" placeholder="Entrez votre ID">
            <label for="txtPlante" class="form-label">Plante :</label>
            <input type="text" name="txtNomPlante" class="form-control" placeholder="Entrez le nom de votre plante">
            <br>
            <label for="region-select">Selectionner une région</label>
                <?php
                    require_once("config/connexion.php");
                    $select = $connexion->query("SELECT * FROM region");
                    $select->setFetchMode(PDO::FETCH_OBJ);
                ?>
            <select name="region" id="region-select">
                
                <?php while($region = $select->fetch()): ?>

                    <option value="<?= $region->noregion ?>"><?= $region->nomregion ?></option>

                <?php endwhile ; ?>

            </select>
            <br>
            <br>
            <label for = "serre-select">Selectionner une serre </label>
            <?php
                    require_once("config/connexion.php");
                    $select = $connexion->query("SELECT * FROM serre");
                    $select->setFetchMode(PDO::FETCH_OBJ);
                ?>
            <select name="serre" id="serre-select">
                
                <?php while($serre = $select->fetch()): ?>

                    <option value="<?= $serre->noserre ?>"><?= $serre->nomserre ?></option>
                    
                <?php endwhile ; ?>

            <a href="views/ajouter_une_plante.php"><input type="submit" name="btnValider" class="btn btn-primary"></a>

        </form>
    </body>
</html>